# -*- coding: utf-8 -*-
##############################################################################
#
#    Thymbra
#    Copyright (C) 2011-2012  Ignacio E. Parszyk <iparszyk@thymbra.com>
#                             Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta

__all__ = ['Move', 'Configuration', 'Location', 'ProductLocationStock',
    'ProductLocationRel', 'Product']
__metaclass__ = PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    surgery = fields.Many2One('gnuhealth.surgery', 'Surgery')
    category = fields.Function(fields.Char('Category'),
        'get_category')

    def get_category(self, name):
        if self.product.category:
            return self.product.category.name


class Configuration(ModelSQL, ModelView):
    'Configuration'
    __name__ = 'product.stock.configuration'

    use_main_storage_loc = fields.Boolean(
        "Use Main Storage for product stocks ?")


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    main_storage_location = fields.Boolean("Main Storage Location")


class ProductLocationStock(ModelView):
    'Product Location Stock'
    __name__ = 'product.locationstock'

    product = fields.Many2One('product.product', 'Product')
    location = fields.Char('Location')
    quantity = fields.Float('Quantity')
    forecast_quantity = fields.Float('Forecast Quantity')


class ProductLocationRel(ModelSQL):
    'Product - Location'
    __name__ = 'product.product-stock.location-tab'

    product = fields.Many2One('product.product', 'Product')
    location = fields.Many2One('stock.location', 'Location')


class Product(metaclass=PoolMeta):
    'Product'
    __name__ = 'product.product'

    moves = fields.One2Many('stock.move', 'product', 'Movimientos')
    quantity_all = fields.Function(fields.Float('Quantity'),
        'get_quantity_all')

    def get_quantity_all(self, name):
        return 0.0
        #Config = Pool().get('product.stock.configuration')
        #Date = Pool().get('ir.date')

        #config = Config(1)
        #if config.use_main_storage_loc:
        #    locations = Pool().get('stock.location').search([
        #        ('main_storage_location', '=', True)
        #    ])
        #else:
        #    locations = Pool().get('stock.location').search([
        #        ('type', '=', 'storage'),
        #        ])

        #Transaction().set_context({'locations': [l.id for l in locations]})

        #if not Transaction().context.get('locations'):
        #    return 0.0

        #context = {}
        #context['stock_date_end'] = Date.today()

        #Transaction().set_context(context)
        #pbl = self.products_by_location(
        #        location_ids=Transaction().context['locations'],
        #        product_ids=[self.id], with_childs=True)

        #res = 0.0
        #for location in Transaction().context['locations']:
        #    res += pbl.get((location, self.id), 0.0)
        #return res
