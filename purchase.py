# -*- coding: utf-8 -*-
##############################################################################
#
#    Thymbra
#    Copyright (C) 2011-2012  Ignacio E. Parszyk <iparszyk@thymbra.com>
#                             Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta

__all__ = ['PurchaseParty', 'Purchase']
__metaclass__ = PoolMeta


class PurchaseParty(ModelSQL):
    'Purchase - Party'
    __name__ = 'purchase.purchase-party.party'
    _table = 'lister_purchase_party_rel'

    purchase = fields.Many2One('purchase.purchase', 'Purchase',
        ondelete='CASCADE', select=True, required=True)
    party = fields.Many2One('party.party', 'Party',
        ondelete='RESTRICT', select=True, required=True)


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    suppliers = fields.Many2Many('purchase.purchase-party.party',
        'purchase', 'party', 'Suppliers')
    apertura_sobres = fields.DateTime('Apertura de sobres')

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls.party.domain = [('supplier', '=', True)]
