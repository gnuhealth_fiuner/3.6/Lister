# -*- coding: utf-8 -*-
##############################################################################
#
#    Thymbra
#    Copyright (C) 2011-2012  Ignacio E. Parszyk <iparszyk@thymbra.com>
#                             Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.pyson import Eval
from trytond.pool import Pool
from decimal import Decimal

__all__ = ['AccountPartida', 'AccountInputPartida', 'AccountEgressOrder',
    'AccountIssuedCheck']


class AccountPartida(ModelSQL, ModelView):
    'Account Partida'
    __name__ = 'account.partida'

    name = fields.Char('Partida', required=True)
    party = fields.Many2One('party.party', 'Party', required=True)
    partida_journal = fields.Many2One('account.journal', 'Journal',
        required=True)
    partida_counterpart_account = fields.Many2One('account.account',
        'Counterpart Account', required=True)
    inputs = fields.One2Many('account.input.partida', 'partida', 'Inputs',
        readonly=True)
    partida_balance = fields.Function(fields.Numeric('Saldo',
        digits=(16, Eval('currency_digits', 2))), 'get_balance')

    def get_balance(self, name):
            return self.partida_journal.debit_account.balance


class AccountInputPartida(Workflow, ModelSQL, ModelView):
    'Account Input Partida'
    __name__ = 'account.input.partida'

    partida = fields.Many2One('account.partida', 'Partida', required=True)
    concept = fields.Char('Concept')
    date = fields.Date('Date', required=True)
    amount = fields.Numeric('Monto', digits=(16, Eval('currency_digits', 2)),
        required=True)
    related_move = fields.Many2One('account.move', 'Move', readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(AccountInputPartida, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
            ))
        cls._buttons.update({
            'done': {
                'invisible': ~Eval('state').in_(['draft']),
                }})

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, partidas):
        Move = Pool().get('account.move')
        MoveLine = Pool().get('account.move.line')
        Period = Pool().get('account.period')

        new_moves = []
        for partida_input in partidas:
            move, = Move.create([{
                'name': partida_input.concept,
                'period': Period.find(1, date=partida_input.date),
                'journal': partida_input.partida.partida_journal.id,
                'date': partida_input.date,
                }])
            new_moves.append({
                'name': partida_input.concept,
                'debit': Decimal(str(partida_input.amount)),
                'credit': Decimal('0.00'),
                'account':
                    partida_input.partida.partida_journal.debit_account.id,
                'move': move.id,
                'journal': partida_input.partida.partida_journal.id,
                'period': Period.find(1, date=partida_input.date),
                'party': partida_input.partida.party.id,
                })
            new_moves.append({
                'name': partida_input.concept,
                'debit': Decimal('0.00'),
                'credit': Decimal(str(partida_input.amount)),
                'account': partida_input.partida.partida_counterpart_account,
                'move': move.id,
                'journal': partida_input.partida.partida_journal.id,
                'period': Period.find(1, date=partida_input.date),
                'party': partida_input.partida.party.id,
                })
            MoveLine.create(new_moves)
            Move.post([move])
            cls.write(cls.id, {'related_move': move.id})
        return True


class AccountIssuedCheck(ModelSQL, ModelView):
    'Account Issued Check'
    __name__ = 'account.issued.check'

    egress_order = fields.Many2One('account.egress.order', 'Egress Order')


class AccountEgressOrder(Workflow, ModelSQL, ModelView):
    'Account Egress Order'
    __name__ = 'account.egress.order'

    party = fields.Many2One('party.party', 'Tercero', required=True)
    concept = fields.Char('Concept')
    journal = fields.Many2One('account.journal', 'Journal')
    account = fields.Many2One('account.account', 'Account')
    date = fields.Date('Date', required=True)
    reference = fields.Char('Reference')
    amount = fields.Numeric('Monto', digits=(16, Eval('currency_digits', 2)))
    issued_checks = fields.One2Many('account.issued.check', 'egress_order',
        'Issued Checks')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(AccountEgressOrder, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update({
            'done': {
                'invisible': ~Eval('state').in_(['draft']),
                }})

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, orders):
        Move = Pool().get('account.move')
        Period = Pool().get('account.period')
        MoveLine = Pool().get('account.move.line')
        IssuedCheck = Pool().get('account.issued.check')

        new_moves = []
        check_amount = Decimal("0.00")
        for order in orders:
            move, = Move.create([{
                'name': order.concept,
                'period': Period.find(1, date=order.date),
                'journal': order.journal.id,
                'date': order.date,
                }])
            for check in order.issued_checks:
                check_amount += check.amount
            new_moves.append({
                'name': order.concept,
                'debit': Decimal("0.00"),
                'credit': Decimal(str(check_amount)),
                'account': order.journal.issued_check_account.id,
                'move': move.id,
                'journal': order.journal.id,
                'period': Period.find(1, date=order.date),
                'party': order.party.id,
                })
            new_moves.append({
                'name': order.concept,
                'debit': Decimal(str(check_amount)),
                'credit': Decimal('0.00'),
                'account': order.account.id,
                'move': move.id,
                'journal': order.journal.id,
                'period': Period.find(1, date=order.date),
                'party': order.party.id,
                })
            MoveLine.create(new_moves)
            Move.post([move])
            if order.issued_checks:
                IssuedCheck.write(order.issued_checks, {
                    'receiving_party': order.party.id,
                    'state': 'issued',
                    })
            return True
