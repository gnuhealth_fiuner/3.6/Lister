# -*- coding: utf-8 -*-
##############################################################################
#
#    Thymbra
#    Copyright (C) 2011-2012  Ignacio E. Parszyk <iparszyk@thymbra.com>
#                             Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import operator
from itertools import groupby
from dateutil.relativedelta import relativedelta, MO
from datetime import timedelta, date, datetime
from sql import Literal, Join, Union
from sql.aggregate import Sum, Count, Max
from sql.conditionals import Case
from sql.functions import Age
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateAction, StateView, Button
from trytond.pyson import Eval, Not, Bool, And, Equal, PYSONEncoder, If
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.exceptions import UserError
from trytond.modules.company import CompanyReport
from trytond import backend
from functools import reduce

__all__ = ['Party', 'SocialPlans', 'PatientSocialPlans', 'PatientData',
    'AppointmentReport', 'GnuHealthPatientLabTest', 'TestType',
    'PatientPregnancy', 'Surgery', 'PatientSurgeryLineMedicament',
    'PatientSurgeryLineInput', 'HealthProfessional', 'ChildhoodStage',
    'ChildhoodTest', 'PatientEvaluation', 'AppointmentsReportEvaluation',
    'PatientEvaluationChildhoodInfantTest',
    'PatientEvaluationChildhoodEarlyTest', 'PatientEvaluationReport',
    'PrintPatientEvaluationReportStart', 'PrintPatientEvaluationReport',
    'Medicament', 'PatientPrescriptionOrder', 'PrescriptionLine',
    'InpatientRegistration', 'InpatientMedication', 'ImagingTestResult', 
    'PaperArchive', 'PrintMedicationDeliveredDayStart', 
    'PrintMedicationDeliveredDay', 'MedicationDeliveredDay',
    'PrintMedicationDeliveredProductStart', 'PrintMedicationDeliveredProduct', 
    'MedicationDeliveredProduct', 'AppointmentsSpecialty', 
    'OpenAppointmentsSpecialtyStart', 'OpenAppointmentsSpecialty', 
    'PracticesReport', 'OpenPracticesReportStart', 'OpenPracticesReport', 
    'HospitalizationReport', 'OpenHospitalizationReportStart', 
    'OpenHospitalizationReport', 'OpenInpatientEvaluations']
__metaclass__ = PoolMeta

_STATES = {
    'readonly': Eval('state') == 'done',
    }
_DEPENDS = ['state']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    ref = fields.Char(
        'PUID',
        help='Person Unique Identifier',
        states={
            'invisible': Not(Bool(Eval('is_person'))),
            'required': And(Bool(Eval('is_person')),
                Not(Bool(Eval('alternative_identification')))),
            })
    supplier = fields.Boolean('Supplier')
    education = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Incomplete Primary School'),
        ('2', 'Primary School'),
        ('3', 'Incomplete Secondary School'),
        ('6', 'Special School'),
        ('4', 'Secondary School'),
        ('5', 'University'),
        ], 'Education Level', help="Education Level", sort=False)
    housing = fields.Selection([
        (None, ''),
        ('0', 'Shanty, deficient sanitary conditions'),
        ('5', 'Precarious conditions'),
        ('1', 'Small, crowded but with good sanitary conditions'),
        ('2', 'Comfortable and good sanitary conditions'),
        ('3', 'Roomy and excellent sanitary conditions'),
        ('4', 'Luxury and excellent sanitary conditions'),
        ], 'Housing conditions', help="Housing and sanitary living conditions",
        sort=False)

    @classmethod
    # Update to version 2.6
    def __register__(cls, module_name):
        super(Party, cls).__register__(module_name)

        cursor = Transaction().connection.cursor()
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        # Remove DNI, use PUID (ref)
        if table.column_exist('dni'):

            cursor.execute(
                'UPDATE party_party '
                'SET ref = dni')

            table.drop_column('dni')


class SocialPlans(ModelSQL, ModelView):
    'Social Plans'
    __name__ = 'gnuhealth.social.plan'

    name = fields.Char('Name', required=True)


class PatientSocialPlans(ModelSQL, ModelView):
    'Patient Social Plans'
    __name__ = 'gnuhealth.patient.social.plans'

    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    social_plan = fields.Many2One('gnuhealth.social.plan', 'Social Plan')
    short_comment = fields.Char('Comment')


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    historia_clinica = fields.Char('HC')
    active = fields.Boolean('Active')
    social_plans = fields.One2Many('gnuhealth.patient.social.plans', 'patient',
        'Social Plans')

    @staticmethod
    def default_active():
        return True


class AppointmentReport(metaclass=PoolMeta):
    __name__ = 'gnuhealth.appointment.report'

    historia_clinica = fields.Char('HC')
    dni = fields.Char('DNI')

    @classmethod
    def table_query(cls):
        pool = Pool()
        appointment = pool.get('gnuhealth.appointment').__table__()
        party = pool.get('party.party').__table__()
        patient = pool.get('gnuhealth.patient').__table__()
        join1 = Join(appointment, patient)
        join1.condition = join1.right.id == appointment.patient
        join2 = Join(join1, party)
        join2.condition = join2.right.id == join1.right.name
        where = Literal(True)
        if Transaction().context.get('date_start'):
            where &= (appointment.appointment_date >=
                    Transaction().context['date_start'])
        if Transaction().context.get('date_end'):
            where &= (appointment.appointment_date <
                    Transaction().context['date_end'] + timedelta(days=1))
        if Transaction().context.get('healthprof'):
            where &= \
                appointment.healthprof == Transaction().context['healthprof']

        return join2.select(
            appointment.id,
            appointment.create_uid,
            appointment.create_date,
            appointment.write_uid,
            appointment.write_date,
            join2.right.ref,
            join1.right.id.as_('patient'),
            join2.right.gender,
            join2.right.ref.as_('dni'),
            join1.right.historia_clinica,
            appointment.appointment_date,
            appointment.appointment_date.as_('appointment_date_time'),
            appointment.healthprof,
            where=where)


class GnuHealthPatientLabTest(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.lab.test'

    patient_status = fields.Function(fields.Char('Hospitalization Status'),
        'get_patient_status')

    def get_patient_status(self, name):
        return  self.patient_id.patient_status


class TestType(metaclass=PoolMeta):
    __name__ = 'gnuhealth.lab.test_type'

    @classmethod
    def __setup__(cls):
        super(TestType, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))


class PatientPregnancy(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.pregnancy'

    pdd = fields.Date('Pregnancy Due Date')
    lmp = fields.Date('LMP', help="Last Menstrual Period",
        on_change=['lmp'], required=False)
    fud = fields.Date('First Ultrasound Date')
    fud_weeks_pregnant = fields.Integer('First Ultrasound Weeks Pregnant',
        on_change=['fud_weeks_pregnant', 'fud'],
        states={
            'invisible': Not(Bool(Eval('fud'))),
            'required': Bool(Eval('current_pregnancy')),
            })

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        cursor = Transaction().connection.cursor()
        table = TableHandler(cls, module_name)

        # drop required on sequence
        table.not_null_action('lmp', action='remove')

        super(PatientPregnancy, cls).__register__(module_name)

    def on_change_lmp(self):
        res = {'pdd': None}
        if self.lmp:
            res['pdd'] = self.lmp + timedelta(days=280)
        return res

    def on_change_fud_weeks_pregnant(self):
        res = {'pdd': None}
        if self.fud_weeks_pregnant:
            res['pdd'] = (self.fud +
                timedelta(weeks=40 - self.fud_weeks_pregnant) +
                relativedelta(weekday=MO(-1)))
        return res


class Surgery(Workflow, ModelSQL, ModelView):
    'Surgery'
    __name__ = 'gnuhealth.surgery'

    date_end = fields.DateTime('Date End')
    first_assistant = fields.Many2One('gnuhealth.healthprofessional',
        'First Assistant')
    second_assistant = fields.Many2One('gnuhealth.healthprofessional',
        'Second Assistant')
    weight = fields.Float('Weight')
    anesthesia_type = fields.Selection([
        ('l', 'Local'),
        ('r', 'Regional'),
        ('g', 'General'),
        ('e', 'Epidural'),
        ('s', 'Spinal'),
        ('n', 'Neuroleptanalgesia'),
        ], 'Anesthesia Type ')
    pathological_anatomy = fields.Boolean('Pathological Anatomy')
    pathological_anatomy_dest = fields.Many2One('party.party', 'Destination',
        states={'invisible': Not(Bool(Eval('pathological_anatomy')))})
    pathological_anatomy_material = fields.Char('Material',
        states={'invisible': Not(Bool(Eval('pathological_anatomy')))})
    medication_line = fields.One2Many(
        'gnuhealth.patient.surgery.line.medicament', 'name', 'Medication')
    medical_input_line = fields.One2Many(
        'gnuhealth.patient.surgery.line.input', 'name', 'Medical Inputs')
    moves = fields.One2Many('stock.move', 'surgery', 'Stock Moves',
        readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ], 'State', readonly=True)
    surgery_warehouse = fields.Many2One('stock.location',
        'Surgery Warehouse', required=True)

    @classmethod
    def __setup__(cls):
        super(Surgery, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update({
            'done': {
            'invisible': ~Eval('state').in_(['draft']),
            }})

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, surgeries):
        lines_to_ship = {}
        medicaments_to_ship = []
        inputs_to_ship = []

        for surgery in surgeries:
            for medicament in surgery.medication_line:
                medicaments_to_ship.append(medicament)

            for medical_input in surgery.medical_input_line:
                inputs_to_ship.append(medical_input)

        lines_to_ship['medicaments'] = medicaments_to_ship
        lines_to_ship['inputs'] = inputs_to_ship

        cls.create_stock_moves(surgeries, lines_to_ship)
        return True

    def create_stock_moves(self, surgeries, lines):
        Move = Pool().get('stock.move')
        Date = Pool().get('ir.date')

        for surgery in surgeries:
            for medicament in lines['medicaments']:
                move_info = {}
                move_info['product'] = medicament.medicament.name.id
                move_info['uom'] = medicament.medicament.name.default_uom.id
                move_info['quantity'] = medicament.quantity
                move_info['from_location'] = \
                    surgery.surgery_warehouse.id
                move_info['to_location'] = \
                    surgery.patient.name.customer_location.id
                move_info['unit_price'] = 1
                move_info['surgery'] = surgery.id
                if medicament.lot.id:
                    if medicament.lot.due_date \
                    and medicament.lot.due_date < Date.today():
                        raise UserError('El/Los medicamento/s que intenta \
                            entregar se encuentra/n vencido/s')
                move_info['lot'] = medicament.lot.id
                new_move, = Move.create([move_info])
                Move.write([new_move], {
                    'state': 'done',
                    'effective_date': Date.today(),
                    })
            for medical_input in lines['inputs']:
                    move_info = {}
                    move_info['product'] = medical_input.product.id
                    move_info['uom'] = medical_input.product.default_uom.id
                    move_info['quantity'] = medical_input.quantity
                    move_info['from_location'] = \
                        surgery.surgery_warehouse.id
                    move_info['to_location'] = \
                        surgery.patient.name.customer_location.id
                    move_info['unit_price'] = 1
                    move_info['surgery'] = surgery.id
                    if medical_input.lot.id:
                        if medical_input.lot.due_date \
                        and medical_input.lot.due_date < Date.today():
                            raise UserError('El/Los insumo/s que intenta \
                                utilizar se encuentra/n vencido/s')
                        move_info['lot'] = medical_input.lot.id
                    new_move, = Move.create([move_info])
                    Move.write([new_move], {
                                    'state': 'done',
                                    'effective_date': Date.today(),
                                })
        return True


class PatientSurgeryLineMedicament(ModelSQL, ModelView):
    'Patient Surgery Line Medicament'
    __name__ = 'gnuhealth.patient.surgery.line.medicament'

    name = fields.Many2One('gnuhealth.surgery', 'Surgery ID')
    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        required=True, on_change=['medicament', 'product'])
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot',
        domain=[('product', '=', Eval('product'))])

    @staticmethod
    def default_quantity():
        return 1

    def on_change_medicament(self):
        return {'product': Pool().get('gnuhealth.medicament').browse(
            self.medicament).name.id}


class PatientSurgeryLineInput(ModelSQL, ModelView):
    'Patient Surgery Line Medical Input'
    __name__ = 'gnuhealth.patient.surgery.line.input'

    name = fields.Many2One('gnuhealth.surgery', 'Surgery ID')
    product = fields.Many2One('product.product', 'Medical Input',
        domain=[('is_medical_supply', '=', True)], required=True)
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot',
        domain=[
            ('product', '=', Eval('product')),
            ])

    @staticmethod
    def default_quantity():
        return 1


class HealthProfessional(metaclass=PoolMeta):
    __name__ = 'gnuhealth.healthprofessional'

    multi_specialty = fields.Boolean('Multi Specialty')


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'

    age = fields.Function(fields.Char('Patient Age'), 'get_patient_age')
    age_months = fields.Function(fields.Char('Patient Age in Months'),
        'get_patient_age_months')
#    childhood_infant_stage = fields.Many2One('gnuhealth.childhood.stage',
#        'Childhood Infant Stage', states=_STATES, depends=_DEPENDS,
#        on_change=['childhood_infant_stage', 'childhood_infant_tests'],
#        domain=[('stage', '=', 'infant')])
#    childhood_infant_tests = fields.One2Many(
#        'gnuhealth.patient.evaluation.childhood.infant.test', 'evaluation',
#        'Childhood Infant Tests', states=_STATES, depends=_DEPENDS)
#    childhood_early_stage = fields.Many2One('gnuhealth.childhood.stage',
#        'Childhood Early Stage', states=_STATES, depends=_DEPENDS,
#        on_change=['childhood_early_stage', 'childhood_early_tests'],
#        domain=[('stage', '=', 'early_childhood')])
#    childhood_early_tests = fields.One2Many(
#        'gnuhealth.patient.evaluation.childhood.early.test', 'evaluation',
#        'Childhood Early Tests', states=_STATES, depends=_DEPENDS)

    def get_patient_age(self, name):
        if self.patient:
            if self.patient.dob:
                delta = relativedelta(self.evaluation_start, self.patient.dob)
                return str(delta.years) + 'y ' \
                        + str(delta.months) + 'm ' \
                        + str(delta.days) + 'd'
        return "Sin Edad"

    def get_patient_age_months(self, name):
        if self.patient:
            if self.patient.dob:
                delta = relativedelta(self.evaluation_start, self.patient.dob)
                return str(delta.years * 12 + delta.months)
        return "Sin Edad"

    def on_change_childhood_infant_stage(self):
        res = {}
        res['childhood_infant_tests'] = {}

        if self.childhood_infant_tests:
            res['childhood_infant_tests']['remove'] = \
                [x['id'] for x in self.childhood_infant_tests]

        if self.childhood_infant_stage:
            for childhood_test in self.childhood_infant_stage.childhood_tests:
                res['childhood_infant_tests'].setdefault('add', []).append(
                    {'test': childhood_test.id})
        return res

    def on_change_childhood_early_stage(self):
        res = {}
        res['childhood_early_tests'] = {}

        if self.childhood_early_tests:
            res['childhood_early_tests']['remove'] = \
                [x['id'] for x in self.childhood_early_tests]

        if self.childhood_early_stage:
            for childhood_test in self.childhood_early_stage.childhood_tests:
                res['childhood_early_tests'].setdefault('add', []).append(
                    {'test': childhood_test.id})
        return res


class PatientEvaluationChildhoodInfantTest(ModelSQL, ModelView):
    'Patient Evaluation Childhood Infant Test'
    __name__ = 'gnuhealth.patient.evaluation.childhood.infant.test'

   # evaluation = fields.Many2One('gnuhealth.patient.evaluation', 'Evaluation',
   #     required=True)
    test = fields.Many2One('gnuhealth.childhood.test', 'Test', required=True)
    result_yes = fields.Boolean('Yes')
    result_no = fields.Boolean('No')


class PatientEvaluationChildhoodEarlyTest(ModelSQL, ModelView):
    'Patient Evaluation Childhood Early Test'
    __name__ = 'gnuhealth.patient.evaluation.childhood.early.test'

   # evaluation = fields.Many2One('gnuhealth.patient.evaluation', 'Evaluation',
   #     required=True)
    test = fields.Many2One('gnuhealth.childhood.test', 'Test', required=True)
    result_yes = fields.Boolean('Yes')
    result_no = fields.Boolean('No')


class ChildhoodStage(ModelSQL, ModelView):
    'Childhood Stage'
    __name__ = 'gnuhealth.childhood.stage'

    name = fields.Char('Name', required=True)
    stage = fields.Selection([
        ('infant', 'Infant'),
        ('early_childhood', 'Early Childhood'),
        ('middle_childhood', 'Middle Childhood'),
        ('adolescence', 'Adolescence'),
        ], 'Childhood Age ', required=True)
    childhood_tests = fields.One2Many('gnuhealth.childhood.test', 'stage',
        'Childhood Tests')


class ChildhoodTest(ModelSQL, ModelView):
    'Childhood Test'
    __name__ = 'gnuhealth.childhood.test'

    name = fields.Char('Name', required=True)
    stage = fields.Many2One('gnuhealth.childhood.stage', 'Stage',
        required=True)


class AppointmentsReportEvaluation(Report):
    __name__ = 'gnuhealth.appointment.report.evaluation'

    @classmethod
    def parse(cls, report, objects, data, localcontext):
        pool = Pool()
        Evaluation = pool.get('gnuhealth.patient.evaluation')

        appointment_evaluations = []
        for appointment in objects:
            evaluations = Evaluation.search([
                    ('evaluation_date', '=', appointment.id),
                    ])
            appointment_evaluations.extend(evaluations)

        objects = appointment_evaluations

        return super(AppointmentsReportEvaluation, cls).parse(report, objects,
            data, localcontext)


class PatientEvaluationReport(CompanyReport):
    __name__ = 'patient.evaluation'

    @classmethod
    def parse(cls, report, objects, data, localcontext):
        pool = Pool()
        Evaluation = pool.get('gnuhealth.patient.evaluation')
        Rounding = pool.get('gnuhealth.patient.rounding')
        AmbulatoryCare = pool.get('gnuhealth.patient.ambulatory_care')

        objects = Evaluation.search([
            ('id', 'in', data['ids']),
            ])

        roundings = Rounding.search([
            ('name.patient', '=', objects[0].patient.id)
            ], order=[('evaluation_start', 'DESC')],
            limit=data['roundings'])
        ambulatorycares = AmbulatoryCare.search([
            ('patient', '=', objects[0].patient.id)
            ], order=[('session_start', 'DESC')],
            limit=data['ambulatorycares'])

        localcontext['roundings'] = roundings
        localcontext['ambulatorycares'] = ambulatorycares

        return super(PatientEvaluationReport, cls).parse(report, objects, data,
            localcontext)


class PrintPatientEvaluationReportStart(ModelView):
    'Print Patient Evaluation and History Start'
    __name__ = 'patient.evaluation.print.start'

    roundings = fields.Integer('Roundings', required=True)
    ambulatorycares = fields.Integer('Ambulatory Cares', required=True)

    @staticmethod
    def default_roundings():
        return 2

    @staticmethod
    def default_ambulatorycares():
        return 2


class PrintPatientEvaluationReport(Wizard):
    'Print Patient Evaluation and History'
    __name__ = 'patient.evaluation.print'

    start = StateView('patient.evaluation.print.start',
        'Lister.print_patient_evaluation_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateAction('Lister.report_patient_evaluation_lister')

    def do_print_(self, action):
        data = {
            'ids': Transaction().context['active_ids'],
            'roundings': self.start.roundings,
            'ambulatorycares': self.start.ambulatorycares,
            }
        return action, data


class Medicament(metaclass=PoolMeta):
    __name__ = 'gnuhealth.medicament'

    prescription_presentation = fields.Many2One('gnuhealth.drug.form',
        'Presentation')
    prescription_route = fields.Many2One('gnuhealth.drug.route', 'Route')
    prescription_dosage = fields.Float('Dosage')
    prescription_unit = fields.Many2One('gnuhealth.dose.unit', 'Unit')

    def get_quantity(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        Location = pool.get('stock.location')
        Product = pool.get('product.product')
        Config = Pool().get('product.stock.configuration')

        config = Config(1)
        if config.use_main_storage_loc:
            locations = Location.search([('main_storage_location', '=', True)])
        else:
            locations = Location.search([('type', '=', 'storage')])

        Transaction().set_context({'locations': [l.id for l in locations]})
        context = {}
        context['stock_date_end'] = Date.today()
        Transaction().set_context(context)

        pbl = Product.products_by_location(
                location_ids=Transaction().context['locations'],
                product_ids=[self.name.id], with_childs=True)
        quantity = 0.00
        if list(pbl.values()):
            quantity = reduce(lambda x, y: x + y, list(pbl.values()))
        return quantity


class PatientPrescriptionOrder(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.order'

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        cls._order.insert(0, ('prescription_id', 'DESC'))


class PrescriptionLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.line'

    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        required=True, help='Prescribed Medicament',
        on_change=['medicament'])

    def on_change_medicament(self):
        res = {
            'form': None,
            'route': None,
            'dose': None,
            'dose_unit': None,
            }
        if self.medicament:
            res['form'] = (self.medicament.prescription_presentation and
                self.medicament.prescription_presentation.id)
            res['route'] = (self.medicament.prescription_route and
                self.medicament.prescription_route.id)
            res['dose'] = self.medicament.prescription_dosage
            res['dose_unit'] = (self.medicament.prescription_unit and
                self.medicament.prescription_unit.id)
        return res


class InpatientRegistration(metaclass=PoolMeta):
    __name__ = 'gnuhealth.inpatient.registration'
    effective_discharge_date = fields.DateTime('Effective Discharge Date',
        readonly=True, states={'invisible': Not(Equal(Eval('state'), 'done'))})

    @classmethod
    def discharge(cls, registrations):
        cls.write(registrations, {'effective_discharge_date': datetime.now()})
        super(InpatientRegistration, cls).discharge(registrations)


class InpatientMedication(metaclass=PoolMeta):
    __name__ = 'gnuhealth.inpatient.medication'

    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        required=True, help='Prescribed Medicament',
        on_change=['medicament'])

    def on_change_medicament(self):
        res = {
            'form': None,
            'route': None,
            'dose': None,
            'dose_unit': None,
            }
        if self.medicament:
            res['form'] = (self.medicament.prescription_presentation and
                self.medicament.prescription_presentation.id)
            res['route'] = (self.medicament.prescription_route and
                self.medicament.prescription_route.id)
            res['dose'] = self.medicament.prescription_dosage
            res['dose_unit'] = (self.medicament.prescription_unit and
                self.medicament.prescription_unit.id)
        return res


class ImagingTestResult(metaclass=PoolMeta):
    __name__ = 'gnuhealth.imaging.test.result'
    plates_number = fields.Integer('Plates Number')


class PaperArchive(metaclass=PoolMeta):
    __name__ = 'gnuhealth.paper_archive'

    @classmethod
    def __setup__(cls):
        '''Remove constraints'''
        super(PaperArchive, cls).__setup__()
        cls._sql_constraints = []

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        super(PaperArchive, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = TableHandler(cls, module_name)
        table.drop_constraint('legacy_uniq')
        table.drop_constraint('patient_uniq')


class PrintMedicationDeliveredDayStart(ModelView):
    'Print Medication Delivered per Day Start'
    __name__ = 'stock.print_medication_delivered_day.start'
    from_date = fields.Date('From Date', required=True)
    to_date = fields.Date('To Date', required=True)

    @staticmethod
    def default_from_date():
        Date = Pool().get('ir.date')
        return date(Date.today().year, Date.today().month, 1)

    @staticmethod
    def default_to_date():
        Date = Pool().get('ir.date')
        return Date.today()


class PrintMedicationDeliveredDay(Wizard):
    'Print Medication Delivered per Day'
    __name__ = 'stock.print_medication_delivered_day'
    start = StateView('stock.print_medication_delivered_day.start',
        'Lister.print_medication_delivered_day_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateAction('Lister.report_medication_delivered_day')

    def do_print_(self, action):
        Medicament = Pool().get('gnuhealth.medicament')

        medicament = Medicament(Transaction().context['active_id'])
        data = {
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
            'product': medicament.name.id,
            }
        return action, data


class MedicationDeliveredDay(Report):
    __name__ = 'stock.medication_delivered_day.report'

    @classmethod
    def parse(cls, report, objects, data, localcontext):
        pool = Pool()
        Move = pool.get('stock.move')
        Product = pool.get('product.product')

        product = Product(data['product'])

        cursor = Transaction().connection.cursor()
        move = Move.__table__()
        where = move.effective_date >= data['from_date']
        where &= move.effective_date <= data['to_date']
        where &= move.product == data['product']
        where &= move.from_location == 3

        cursor.execute(*move.select(
            move.effective_date,
            Sum(move.quantity).as_('sum'),
            where=where,
            group_by=move.effective_date,
            order_by=move.effective_date))
        dates = cursor.fetchall()

        date2lines = cls.lines(dates, product)

        total_quantity = 0
        for d in dates:
            total_quantity += d[1]

        localcontext['dates'] = dates
        localcontext['product'] = product
        localcontext['from_date'] = data['from_date']
        localcontext['to_date'] = data['to_date']
        localcontext['lines'] = lambda date: date2lines[date]
        localcontext['total_quantity'] = total_quantity

        return super(MedicationDeliveredDay, cls).parse(report, objects, data,
            localcontext)

    @classmethod
    def get_lines(cls, dates, product):
        Move = Pool().get('stock.move')
        clause = [
            ('effective_date', 'in', [d[0] for d in dates]),
            ('product', '=', product),
            ('from_location.code', '=', 'DEPFAR'),
            ]
        lines = Move.search(clause)
        return groupby(lines, operator.attrgetter('effective_date'))

    @classmethod
    def lines(cls, dates, product):
        res = dict((d[0], []) for d in dates)
        date2lines = cls.get_lines(dates, product)

        for effective_date, lines in date2lines:
            for line in lines:
                res[effective_date].append({
                        'quantity': line.quantity,
                        'origin': line.origin.rec_name if line.origin else '',
                        'to_location': line.to_location.name,
                        })
        return res


class PrintMedicationDeliveredProductStart(ModelView):
    'Print Medication Delivered per Product Start'
    __name__ = 'stock.print_medication_delivered_product.start'
    from_date = fields.Date('From Date', required=True)
    to_date = fields.Date('To Date', required=True)

    @staticmethod
    def default_from_date():
        Date = Pool().get('ir.date')
        return date(Date.today().year, Date.today().month, 1)

    @staticmethod
    def default_to_date():
        Date = Pool().get('ir.date')
        return Date.today()


class PrintMedicationDeliveredProduct(Wizard):
    'Print Medication Delivered per Product'
    __name__ = 'stock.print_medication_delivered_product'
    start = StateView('stock.print_medication_delivered_product.start',
        'Lister.print_medication_delivered_product_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateAction('Lister.report_medication_delivered_product')

    def do_print_(self, action):
        Medicament = Pool().get('gnuhealth.medicament')

        medicaments = Medicament.search([
            ('id', 'in', Transaction().context['active_ids'])
            ])
        data = {
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
            'products': [medicament.name.id for medicament in medicaments],
            }
        return action, data


class MedicationDeliveredProduct(Report):
    __name__ = 'stock.medication_delivered_product.report'

    @classmethod
    def parse(cls, report, objects, data, localcontext):
        pool = Pool()
        Move = pool.get('stock.move')
        Product = pool.get('product.product')
        Template = pool.get('product.template')

        cursor = Transaction().connection.cursor()
        move = Move.__table__()
        product = Product.__table__()
        template = Template.__table__()
        where = move.effective_date >= data['from_date']
        where &= move.effective_date <= data['to_date']
        where &= move.from_location == 3

        join = Join(move, product)
        join.condition = join.right.id == move.product
        join2 = Join(join, template)
        join2.condition = join.right.template == join2.right.id

        cursor.execute(*join2.select(
            join2.right.name,
            Sum(move.quantity).as_('sum'),
            where=where,
            group_by=join2.right.name,
            order_by=join2.right.name))
        products = cursor.fetchall()

        total_quantity = 0
        for p in products:
            total_quantity += p[1]

        localcontext['products'] = products
        localcontext['from_date'] = data['from_date']
        localcontext['to_date'] = data['to_date']
        localcontext['total_quantity'] = total_quantity

        return super(MedicationDeliveredProduct, cls).parse(report, objects,
            data, localcontext)


#class EvaluationsSpecialty(metaclass=PoolMeta):
    #__name__ = 'gnuhealth.evaluations_specialty'

    #m_l_1 = fields.Integer('M lw 1')
    #f_l_1 = fields.Integer('F lw 1')
    #m_1 = fields.Integer('M 1')
    #f_1 = fields.Integer('F 1')
    #m_2_4 = fields.Integer('M 2 - 4')
    #f_2_4 = fields.Integer('F 2 - 4')
    #m_5_9 = fields.Integer('M 5 - 9')
    #f_5_9 = fields.Integer('F 5 - 9')
    #m_10_14 = fields.Integer('M 10 - 14')
    #f_10_14 = fields.Integer('F 10 - 14')
    #m_15_49 = fields.Integer('M 15 - 49')
    #f_15_49 = fields.Integer('F 15 - 49')
    #m_g_50 = fields.Integer('M gte 50')
    #f_g_50 = fields.Integer('F gte 50')
    #nfi = fields.Integer('Not filled in')
    #insured = fields.Function(fields.Char('Insured'), 'get_insured')
    #uninsured = fields.Function(fields.Char('Uninsured'), 'get_uninsured')

    #def get_insured(self, name):
        #cursor = Transaction().connection.cursor()
        #if self.id != 32:
            #clause = ' '
            #args = []
            #if Transaction().context.get('start_date'):
                #clause += 'AND e.evaluation_start >= %s '
                #args.append(Transaction().context['start_date'])
            #if Transaction().context.get('end_date'):
                #clause += 'AND e.evaluation_start <= %s '
                #args.append(Transaction().context['end_date'])
            #args.append(self.id)
            #cursor.execute("""
                #select count(*)
                #from gnuhealth_patient_evaluation e,
                    #gnuhealth_patient p,
                    #party_party r
                #where e.patient = p.id
                #and p.name = r.id
                #and (select count(*) from gnuhealth_insurance
                    #where name = r.id) > 0 """
                #+ clause + """
                #and e.specialty = %s""", args)
        #else:
            #clause = ' '
            #args = []
            #if Transaction().context.get('start_date'):
                #clause += 'AND e.session_start >= %s '
                #args.append(Transaction().context['start_date'])
            #if Transaction().context.get('end_date'):
                #clause += 'AND e.session_start <= %s '
                #args.append(Transaction().context['end_date'])
            #cursor.execute("""
                #select count(*)
                #from gnuhealth_patient_ambulatory_care e,
                    #gnuhealth_patient p,
                    #party_party r
                #where e.patient = p.id
                #and p.name = r.id
                #and (select count(*) from gnuhealth_insurance
                    #where name = r.id) > 0 """
                #+ clause, args)
        #insured, = cursor.fetchone()
        #return insured

    #def get_uninsured(self, name):
        #cursor = Transaction().connection.cursor()
        #if self.id != 32:
            #clause = ' '
            #args = []
            #if Transaction().context.get('start_date'):
                #clause += 'AND e.evaluation_start >= %s '
                #args.append(Transaction().context['start_date'])
            #if Transaction().context.get('end_date'):
                #clause += 'AND e.evaluation_start <= %s '
                #args.append(Transaction().context['end_date'])
            #args.append(self.id)
            #cursor.execute("""
                #select count(*)
                #from gnuhealth_patient_evaluation e,
                    #gnuhealth_patient p,
                    #party_party r
                #where e.patient = p.id
                #and p.name = r.id
                #and (select count(*) from gnuhealth_insurance
                    #where name = r.id) = 0 """
                #+ clause + """
                #and e.specialty = %s""", args)
        #else:
            #clause = ' '
            #args = []
            #if Transaction().context.get('start_date'):
                #clause += 'AND e.session_start >= %s '
                #args.append(Transaction().context['start_date'])
            #if Transaction().context.get('end_date'):
                #clause += 'AND e.session_start <= %s '
                #args.append(Transaction().context['end_date'])
            #cursor.execute("""
                #select count(*)
                #from gnuhealth_patient_ambulatory_care e,
                    #gnuhealth_patient p,
                    #party_party r
                #where e.patient = p.id
                #and p.name = r.id
                #and (select count(*) from gnuhealth_insurance
                    #where name = r.id) = 0 """
                #+ clause, args)
        #insured, = cursor.fetchone()
        #return insured

    #@classmethod
    #def table_query(cls):
        #clause = ' '
        #clause2 = ' '
        #args = []
        #if Transaction().context.get('start_date'):
            #clause += 'AND e.evaluation_start >= %s '
            #args.append(Transaction().context['start_date'])
        #if Transaction().context.get('end_date'):
            #clause += 'AND e.evaluation_start <= %s '
            #args.append(Transaction().context['end_date'])
        #if Transaction().context.get('start_date'):
            #clause2 += 'AND e.session_start >= %s '
            #args.append(Transaction().context['start_date'])
        #if Transaction().context.get('end_date'):
            #clause2 += 'AND e.session_start <= %s '
            #args.append(Transaction().context['end_date'])
        #return ("""
            #SELECT DISTINCT(e.specialty) AS id,
                #MAX(e.create_uid) AS create_uid,
                #MAX(e.create_date) AS create_date,
                #MAX(e.write_uid) AS write_uid,
                #MAX(e.write_date) AS write_date,
                #e.specialty,
                #sum(CASE WHEN age_1 < '1 year' and p.gender = 'm' THEN 1 else 0 end) as "m_l_1",
                #sum(CASE WHEN age_1 < '1 year' and p.gender = 'f' THEN 1 else 0 end) as "f_l_1",
                #sum(CASE WHEN age_1 BETWEEN '1 year' and '2 years' and p.gender = 'm' THEN 1 else 0 end) as "m_1",
                #sum(CASE WHEN age_1 BETWEEN '1 year' and '2 years' and p.gender = 'f' THEN 1 else 0 end) as "f_1",
                #sum(CASE WHEN age_1 BETWEEN '2 years' and '5 years' and p.gender = 'm' THEN 1 else 0 end) as "m_2_4",
                #sum(CASE WHEN age_1 BETWEEN '2 years' and '5 years' and p.gender = 'f' THEN 1 else 0 end) as "f_2_4",
                #sum(CASE WHEN age_1 BETWEEN '5 years' and '10 years' and p.gender = 'm' THEN 1 else 0 end) as "m_5_9",
                #sum(CASE WHEN age_1 BETWEEN '5 years' and '10 years' and p.gender = 'f' THEN 1 else 0 end) as "f_5_9",
                #sum(CASE WHEN age_1 BETWEEN '10 years' and '16 years' and p.gender = 'm' THEN 1 else 0 end) as "m_10_14",
                #sum(CASE WHEN age_1 BETWEEN '10 years' and '16 years' and p.gender = 'f' THEN 1 else 0 end) as "f_10_14",
                #sum(CASE WHEN age_1 BETWEEN '16 years' and '50 years' and p.gender = 'm' THEN 1 else 0 end) as "m_15_49",
                #sum(CASE WHEN age_1 BETWEEN '16 years' and '50 years' and p.gender = 'f' THEN 1 else 0 end) as "f_15_49",
                #sum(CASE WHEN age_1 >= '50 years' and p.gender = 'm' THEN 1 else 0 end) as "m_g_50",
                #sum(CASE WHEN age_1 >= '50 years' and p.gender = 'f' THEN 1 else 0 end) as "f_g_50",
                #sum(CASE WHEN age_1 IS NULL THEN 1 else 0 end) as "nfi",
                #count(*) as "evaluations"
            #FROM gnuhealth_patient_evaluation e, gnuhealth_patient pat,
                #(SELECT age(dob) AS age_1, id, gender FROM party_party) as p
            #WHERE e.patient = pat.id
            #AND pat.name = p.id
            #AND e.specialty <> 32 """
            #+ clause +
            #"""GROUP BY e.specialty
            #UNION
            #SELECT 32 AS id,
                #MAX(e.create_uid) AS create_uid,
                #MAX(e.create_date) AS create_date,
                #MAX(e.write_uid) AS write_uid,
                #MAX(e.write_date) AS write_date,
                #32 as specialty,
                #sum(CASE WHEN age_1 < '1 year' and p.gender = 'm' THEN 1 else 0 end) as "m_l_1",
                #sum(CASE WHEN age_1 < '1 year' and p.gender = 'f' THEN 1 else 0 end) as "f_l_1",
                #sum(CASE WHEN age_1 BETWEEN '1 year' and '2 years' and p.gender = 'm' THEN 1 else 0 end) as "m_1",
                #sum(CASE WHEN age_1 BETWEEN '1 year' and '2 years' and p.gender = 'f' THEN 1 else 0 end) as "f_1",
                #sum(CASE WHEN age_1 BETWEEN '2 years' and '5 years' and p.gender = 'm' THEN 1 else 0 end) as "m_2_4",
                #sum(CASE WHEN age_1 BETWEEN '2 years' and '5 years' and p.gender = 'f' THEN 1 else 0 end) as "f_2_4",
                #sum(CASE WHEN age_1 BETWEEN '5 years' and '10 years' and p.gender = 'm' THEN 1 else 0 end) as "m_5_9",
                #sum(CASE WHEN age_1 BETWEEN '5 years' and '10 years' and p.gender = 'f' THEN 1 else 0 end) as "f_5_9",
                #sum(CASE WHEN age_1 BETWEEN '10 years' and '16 years' and p.gender = 'm' THEN 1 else 0 end) as "m_10_14",
                #sum(CASE WHEN age_1 BETWEEN '10 years' and '16 years' and p.gender = 'f' THEN 1 else 0 end) as "f_10_14",
                #sum(CASE WHEN age_1 BETWEEN '16 years' and '50 years' and p.gender = 'm' THEN 1 else 0 end) as "m_15_49",
                #sum(CASE WHEN age_1 BETWEEN '16 years' and '50 years' and p.gender = 'f' THEN 1 else 0 end) as "f_15_49",
                #sum(CASE WHEN age_1 >= '50 years' and p.gender = 'm' THEN 1 else 0 end) as "m_g_50",
                #sum(CASE WHEN age_1 >= '50 years' and p.gender = 'f' THEN 1 else 0 end) as "f_g_50",
                #sum(CASE WHEN age_1 IS NULL THEN 1 else 0 end) as "nfi",
                #count(*) as "evaluations"
            #FROM gnuhealth_patient_ambulatory_care e, gnuhealth_patient pat,
                #(SELECT age(dob) AS age_1, id, gender FROM party_party) as p
            #WHERE e.patient = pat.id
            #AND pat.name = p.id"""
            #+ clause2, args)


class AppointmentsSpecialty(ModelSQL, ModelView):
    'Appointments per Specialty'
    __name__ = 'gnuhealth.appointments_specialty'

    specialty = fields.Many2One('gnuhealth.specialty', 'Specialty')
    appointments = fields.Integer('Appointments')
    m_l_1 = fields.Integer('M lw 1')
    f_l_1 = fields.Integer('F lw 1')
    m_1 = fields.Integer('M 1')
    f_1 = fields.Integer('F 1')
    m_2_4 = fields.Integer('M 2 - 4')
    f_2_4 = fields.Integer('F 2 - 4')
    m_5_9 = fields.Integer('M 5 - 9')
    f_5_9 = fields.Integer('F 5 - 9')
    m_10_14 = fields.Integer('M 10 - 14')
    f_10_14 = fields.Integer('F 10 - 14')
    m_15_49 = fields.Integer('M 15 - 49')
    f_15_49 = fields.Integer('F 15 - 49')
    m_g_50 = fields.Integer('M gte 50')
    f_g_50 = fields.Integer('F gte 50')
    nfi = fields.Integer('Not filled in')
    insured = fields.Function(fields.Char('Insured'), 'get_insured')
    uninsured = fields.Function(fields.Char('Uninsured'), 'get_uninsured')

    def get_insured(self, name):
        cursor = Transaction().connection.cursor()
        if self.id != 32:
            clause = ' '
            args = []
            if Transaction().context.get('start_date'):
                clause += 'AND a.appointment_date >= %s '
                args.append(Transaction().context['start_date'])
            if Transaction().context.get('end_date'):
                clause += 'AND a.appointment_date <= %s '
                args.append(Transaction().context['end_date'])
            args.append(self.id)
            cursor.execute("""
                select count(*)
                from gnuhealth_appointment a,
                    gnuhealth_patient p,
                    party_party r
                where a.patient = p.id
                and p.name = r.id
                and (select count(*) from gnuhealth_insurance
                    where name = r.id) > 0 """
                + clause + """
                and a.speciality = %s""", args)
        else:
            clause = ' '
            args = []
            if Transaction().context.get('start_date'):
                clause += 'AND e.session_start >= %s '
                args.append(Transaction().context['start_date'])
            if Transaction().context.get('end_date'):
                clause += 'AND e.session_start <= %s '
                args.append(Transaction().context['end_date'])
            cursor.execute("""
                select count(*)
                from gnuhealth_patient_ambulatory_care e,
                    gnuhealth_patient p,
                    party_party r
                where e.patient = p.id
                and p.name = r.id
                and (select count(*) from gnuhealth_insurance
                    where name = r.id) > 0 """
                + clause, args)
        insured, = cursor.fetchone()
        return insured

    def get_uninsured(self, name):
        cursor = Transaction().connection.cursor()
        if self.id != 32:
            clause = ' '
            args = []
            if Transaction().context.get('start_date'):
                clause += 'AND a.appointment_date >= %s '
                args.append(Transaction().context['start_date'])
            if Transaction().context.get('end_date'):
                clause += 'AND a.appointment_date <= %s '
                args.append(Transaction().context['end_date'])
            args.append(self.id)
            cursor.execute("""
                select count(*)
                from gnuhealth_appointment a,
                    gnuhealth_patient p,
                    party_party r
                where a.patient = p.id
                and p.name = r.id
                and (select count(*) from gnuhealth_insurance
                    where name = r.id) = 0 """
                + clause + """
                and a.speciality = %s""", args)
        else:
            clause = ' '
            args = []
            if Transaction().context.get('start_date'):
                clause += 'AND e.session_start >= %s '
                args.append(Transaction().context['start_date'])
            if Transaction().context.get('end_date'):
                clause += 'AND e.session_start <= %s '
                args.append(Transaction().context['end_date'])
            cursor.execute("""
                select count(*)
                from gnuhealth_patient_ambulatory_care e,
                    gnuhealth_patient p,
                    party_party r
                where e.patient = p.id
                and p.name = r.id
                and (select count(*) from gnuhealth_insurance
                    where name = r.id) = 0 """
                + clause, args)
        insured, = cursor.fetchone()
        return insured

    @classmethod
    def table_query(cls):
        pool = Pool()
        appointment = pool.get('gnuhealth.appointment').__table__()
        party = pool.get('party.party').__table__()
        patient = pool.get('gnuhealth.patient').__table__()
        ambulatory_care = \
            pool.get('gnuhealth.patient.ambulatory_care').__table__()

        join1 = Join(appointment, patient)
        join1.condition = join1.right.id == appointment.patient
        join2 = Join(join1, party)
        join2.condition = join2.right.id == join1.right.name
        join3 = Join(ambulatory_care, patient)
        join3.condition = join1.right.id == ambulatory_care.patient
        join4 = Join(join3, party)
        join4.condition = join4.right.id == join3.right.name
        where1 = appointment.speciality != 32
        where2 = Literal(True)
        if Transaction().context.get('start_date'):
            where1 &= appointment.appointment_date >= \
                Transaction().context['start_date']
            where2 &= ambulatory_care.session_start >= \
                Transaction().context['start_date']
        if Transaction().context.get('end_date'):
            where1 &= appointment.appointment_date <= \
                Transaction().context['end_date']
            where2 &= ambulatory_care.session_start <= \
                Transaction().context['end_date']

        query1 = join2.select(
            appointment.speciality.as_('id'),
            Max(appointment.create_uid).as_('create_uid'),
            Max(appointment.create_date).as_('create_date'),
            Max(appointment.write_uid).as_('write_uid'),
            Max(appointment.write_date).as_('write_date'),
            appointment.speciality.as_('specialty'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) < '1 year')
                & (party.gender == 'm')), 1), else_=0)).as_('m_l_1'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) < '1 year')
                & (party.gender == 'f')), 1), else_=0)).as_('f_l_1'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '1 year') & (Age(appointment.appointment_date, party.dob) <
                '2 years') & (party.gender == 'm')), 1), else_=0)).as_('m_1'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '1 year') & (Age(appointment.appointment_date, party.dob) <
                '2 years') & (party.gender == 'f')), 1), else_=0)).as_('f_1'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '2 years') & (Age(appointment.appointment_date, party.dob) <
                '5 years') & (party.gender == 'm')), 1), else_=0)).as_('m_2_4'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '2 years') & (Age(appointment.appointment_date, party.dob) <
                '5 years') & (party.gender == 'f')), 1), else_=0)).as_('f_2_4'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '5 years') & (Age(appointment.appointment_date, party.dob) <
                '10 years') & (party.gender == 'm')), 1), else_=0)).as_('m_5_9'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '5 years') & (Age(appointment.appointment_date, party.dob) <
                '10 years') & (party.gender == 'f')), 1), else_=0)).as_('f_5_9'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '10 years') & (Age(appointment.appointment_date, party.dob) <
                '16 years') & (party.gender == 'm')), 1), else_=0)).as_('m_10_14'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '10 years') & (Age(appointment.appointment_date, party.dob) <
                '16 years') & (party.gender == 'f')), 1), else_=0)).as_('f_10_14'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '16 years') & (Age(appointment.appointment_date, party.dob) <
                '50 years') & (party.gender == 'm')), 1), else_=0)).as_('m_15_49'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '16 years') & (Age(appointment.appointment_date, party.dob) <
                '50 years') & (party.gender == 'f')), 1), else_=0)).as_('f_15_49'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '50 years') & (party.gender == 'm')), 1), else_=0)).as_('m_g_50'),
            Sum(Case((((Age(appointment.appointment_date, party.dob) >=
                '50 years') & (party.gender == 'f')), 1), else_=0)).as_('f_g_50'),
            Sum(Case(((party.dob == None), 1), else_=0)).as_('nfi'),
            Count(appointment.speciality).as_('appointments'),
            where=where1,
            group_by=appointment.speciality)

        query2 = join4.select(
            Literal(32).as_('id'),
            Max(ambulatory_care.create_uid).as_('create_uid'),
            Max(ambulatory_care.create_date).as_('create_date'),
            Max(ambulatory_care.write_uid).as_('write_uid'),
            Max(ambulatory_care.write_date).as_('write_date'),
            Literal(32).as_('specialty'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) <
                '1 year') & (party.gender == 'm')), 1), else_=0)).as_('m_l_1'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) <
                '1 year') & (party.gender == 'f')), 1), else_=0)).as_('f_l_1'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '1 year') & (Age(ambulatory_care.session_start, party.dob) <
                '2 years') & (party.gender == 'm')), 1), else_=0)).as_('m_1'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '1 year') & (Age(ambulatory_care.session_start, party.dob) <
                '2 years') & (party.gender == 'f')), 1), else_=0)).as_('f_1'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '2 years') & (Age(ambulatory_care.session_start, party.dob) <
                '5 years') & (party.gender == 'm')), 1), else_=0)).as_('m_2_4'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '2 years') & (Age(ambulatory_care.session_start, party.dob) <
                '5 years') & (party.gender == 'f')), 1), else_=0)).as_('f_2_4'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '5 years') & (Age(ambulatory_care.session_start, party.dob) <
                '10 years') & (party.gender == 'm')), 1), else_=0)).as_('m_5_9'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '5 years') & (Age(ambulatory_care.session_start, party.dob) <
                '10 years') & (party.gender == 'f')), 1), else_=0)).as_('f_5_9'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '10 years') & (Age(ambulatory_care.session_start, party.dob) <
                '16 years') & (party.gender == 'm')), 1), else_=0)).as_('m_10_14'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '10 years') & (Age(ambulatory_care.session_start, party.dob) <
                '16 years') & (party.gender == 'f')), 1), else_=0)).as_('f_10_14'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '16 years') & (Age(ambulatory_care.session_start, party.dob) <
                '50 years') & (party.gender == 'm')), 1), else_=0)).as_('m_15_49'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '16 years') & (Age(ambulatory_care.session_start, party.dob) <
                '50 years') & (party.gender == 'f')), 1), else_=0)).as_('f_15_49'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '50 years') & (party.gender == 'm')), 1), else_=0)).as_('m_g_50'),
            Sum(Case((((Age(ambulatory_care.session_start, party.dob) >=
                '50 years') & (party.gender == 'f')), 1), else_=0)).as_('f_g_50'),
            Sum(Case(((party.dob == None), 1), else_=0)).as_('nfi'),
            Count(ambulatory_care.id).as_('appointments'),
            where=where2)

        return query1 | query2


class OpenAppointmentsSpecialtyStart(ModelView):
    'Open Appointments per Specialty'
    __name__ = 'gnuhealth.appointments_specialty.open.start'
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')


class OpenAppointmentsSpecialty(Wizard):
    'Open Appointments per Specialty'
    __name__ = 'gnuhealth.appointments_specialty.open'

    start = StateView('gnuhealth.appointments_specialty.open.start',
        'Lister.appointments_specialty_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open_', 'tryton-ok', default=True),
            ])
    open_ = StateAction('Lister.act_appointments_specialty_form')

    def do_open_(self, action):
        action['pyson_context'] = PYSONEncoder().encode({
                'start_date': self.start.start_date,
                'end_date': self.start.end_date,
                })
        return action, {}

    def transition_open_(self):
        return 'end'


class PracticesReport(ModelSQL, ModelView):
    'Practices Report'
    __name__ = 'gnuhealth.practices.report'

    ref = fields.Char('PUID')
    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Health Prof')
    specialty = fields.Many2One('gnuhealth.specialty', 'Specialty')
    diagnosis = fields.Many2One('gnuhealth.pathology', 'Presumptive Diagnosis')
    age = fields.Function(fields.Char('Age'), 'get_patient_age')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], 'gender')
    address = fields.Function(fields.Char('Address'), 'get_address')
    insurance = fields.Function(fields.Char('Insurance'), 'get_insurance')
    number = fields.Char('Number')
    date = fields.Date('Date')
    kind = fields.Selection([
        ('evaluation', 'Evaluation'),
        ('laboratory', 'Laboratory'),
        ('imaging', 'Imaging'),
        ], 'Kind')
    test = fields.Char('Test')

    @classmethod
    def __setup__(cls):
        super(PracticesReport, cls).__setup__()
        cls._order.insert(0, ('date', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        evaluation = pool.get('gnuhealth.patient.evaluation').__table__()
        party = pool.get('party.party').__table__()
        patient = pool.get('gnuhealth.patient').__table__()
        insurance = pool.get('gnuhealth.insurance').__table__()
        lab = pool.get('gnuhealth.lab').__table__()
        imaging = pool.get('gnuhealth.imaging.test.result').__table__()
        lab_test = pool.get('gnuhealth.lab.test_type').__table__()
        imaging_test = pool.get('gnuhealth.imaging.test').__table__()

        join1 = Join(evaluation, patient)
        join1.condition = evaluation.patient == join1.right.id
        join2 = Join(join1, party)
        join2.condition = join1.right.name == join2.right.id
        join3 = Join(join2, insurance)
        join3.condition = join1.right.current_insurance == join3.right.id
        where = Literal(True)
        if Transaction().context.get('date_start'):
            where &= (evaluation.evaluation_start >=
                    Transaction().context['date_start'])
        if Transaction().context.get('date_end'):
            where &= (evaluation.evaluation_start <
                    Transaction().context['date_end'] + timedelta(days=1))
        if Transaction().context.get('healthprof'):
            where &= \
                evaluation.healthprof == Transaction().context['healthprof']
        if Transaction().context.get('insurance'):
            where &= \
                join3.right.company == Transaction().context['insurance']
        if Transaction().context.get('patient'):
            where &= \
                evaluation.patient == Transaction().context['patient']

        query1 = join3.select(
            evaluation.id,
            evaluation.create_uid,
            evaluation.create_date,
            evaluation.write_uid,
            evaluation.write_date,
            join2.right.ref,
            join1.right.id.as_('patient'),
            join3.right.number,
            join2.right.gender,
            evaluation.evaluation_start.as_('date'),
            evaluation.healthprof,
            evaluation.specialty,
            evaluation.diagnosis,
            Literal('evaluation').as_('kind'),
            Literal(None).as_('test'),
            where=where)

        join4 = Join(lab, patient)
        join4.condition = lab.patient == join4.right.id
        join5 = Join(join4, party)
        join5.condition = join4.right.name == join5.right.id
        join6 = Join(join5, insurance)
        join6.condition = join4.right.current_insurance == join6.right.id
        join10 = Join(join6, lab_test)
        join10.condition = join4.left.test == join10.right.id
        where = Literal(True)
        if Transaction().context.get('date_start'):
            where &= (lab.date_analysis >=
                    Transaction().context['date_start'])
        if Transaction().context.get('date_end'):
            where &= (lab.date_analysis <
                    Transaction().context['date_end'] + timedelta(days=1))
        if Transaction().context.get('healthprof'):
            where &= \
                lab.requestor == Transaction().context['healthprof']
        if Transaction().context.get('insurance'):
            where &= \
                join6.right.company == Transaction().context['insurance']
        if Transaction().context.get('patient'):
            where &= \
                lab.patient == Transaction().context['patient']

        query2 = join10.select(
            lab.id,
            lab.create_uid,
            lab.create_date,
            lab.write_uid,
            lab.write_date,
            join5.right.ref,
            join4.right.id.as_('patient'),
            join6.right.number,
            join5.right.gender,
            lab.date_analysis.as_('date'),
            lab.requestor.as_('healthprof'),
            Literal(None).as_('specialty'),
            Literal(None).as_('diagnosis'),
            Literal('laboratory').as_('kind'),
            join10.right.name.as_('test'),
            where=where)

        join7 = Join(imaging, patient)
        join7.condition = imaging.patient == join7.right.id
        join8 = Join(join7, party)
        join8.condition = join7.right.name == join8.right.id
        join9 = Join(join8, insurance)
        join9.condition = join7.right.current_insurance == join9.right.id
        join11 = Join(join9, imaging_test)
        join11.condition = join7.left.requested_test == join11.right.id
        where = Literal(True)
        if Transaction().context.get('date_start'):
            where &= (imaging.date >=
                    Transaction().context['date_start'])
        if Transaction().context.get('date_end'):
            where &= (imaging.date <
                    Transaction().context['date_end'] + timedelta(days=1))
        if Transaction().context.get('healthprof'):
            where &= \
                imaging.doctor == Transaction().context['healthprof']
        if Transaction().context.get('insurance'):
            where &= \
                join9.right.company == Transaction().context['insurance']
        if Transaction().context.get('patient'):
            where &= \
                imaging.patient == Transaction().context['patient']

        query3 = join11.select(
            imaging.id,
            imaging.create_uid,
            imaging.create_date,
            imaging.write_uid,
            imaging.write_date,
            join8.right.ref,
            join7.right.id.as_('patient'),
            join9.right.number,
            join8.right.gender,
            imaging.date.as_('date'),
            imaging.doctor.as_('healthprof'),
            Literal(None).as_('specialty'),
            Literal(None).as_('diagnosis'),
            Literal('imaging').as_('kind'),
            join11.right.name.as_('test'),
            where=where)

        return Union(query1, query2, query3)

    def get_address(self, name):
        res = ''
        if self.patient.name.addresses:
            res = self.patient.name.addresses[0].full_address
        return res

    def get_insurance(self, name):
        res = ''
        if self.patient.current_insurance:
            res = self.patient.current_insurance.company.name
        return res

    def get_patient_age(self, name):
        if self.patient:
            if self.patient.dob:
                delta = relativedelta(self.date, self.patient.dob)
                return str(delta.years) + 'y ' \
                        + str(delta.months) + 'm ' \
                        + str(delta.days) + 'd'
        return "Sin Edad"


class OpenPracticesReportStart(ModelView):
    'Open Practices Report'
    __name__ = 'gnuhealth.practices.report.open.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Health Prof')
    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    insurance = fields.Many2One('party.party', 'Insurance',
        domain=([('is_insurance_company', '=', True)]))

    @staticmethod
    def default_date_start():
        return datetime.now()

    @staticmethod
    def default_date_end():
        return datetime.now()


class OpenPracticesReport(Wizard):
    'Open Practices Report'
    __name__ = 'gnuhealth.practices.report.open'

    start = StateView(
        'gnuhealth.practices.report.open.start',
        'Lister.practices_report_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open_', 'tryton-ok', default=True),
            ])
    open_ = StateAction('Lister.act_practices_report_view_tree')

    def do_open_(self, action):
        context = {}
        context['date_start'] = self.start.date_start
        context['date_end'] = self.start.date_end
        if self.start.healthprof:
            context['healthprof'] = self.start.healthprof.id
        if self.start.insurance:
            context['insurance'] = self.start.insurance.id
        if self.start.patient:
            context['patient'] = self.start.patient.id
        action['pyson_context'] = PYSONEncoder().encode(context)
        return action, {}

    def transition_open_(self):
        return 'end'


class HospitalizationReport(ModelSQL, ModelView):
    'Hospitalization Report'
    __name__ = 'gnuhealth.hospitalization.report'

    name = fields.Char('Registration Code')
    ref = fields.Char('PUID')
    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    attending_physician = fields.Many2One('gnuhealth.healthprofessional',
        'Attending Physician')
    admission_reason = fields.Many2One('gnuhealth.pathology',
        'Reason for Admission')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], 'gender')
    address = fields.Function(fields.Char('Address'), 'get_address')
    insurance = fields.Function(fields.Char('Insurance'), 'get_insurance')
    number = fields.Char('Number')
    hospitalization_date = fields.DateTime('Hospitalization date')
    discharge_date = fields.DateTime('Discharge date')
    state = fields.Selection([
        ('hospitalized', 'hospitalized'),
        ('done', 'Done'),
        ], 'Status')

    @classmethod
    def __setup__(cls):
        super(HospitalizationReport, cls).__setup__()
        cls._order.insert(0, ('hospitalization_date', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        hospitalization = pool.get(
            'gnuhealth.inpatient.registration').__table__()
        party = pool.get('party.party').__table__()
        patient = pool.get('gnuhealth.patient').__table__()
        insurance = pool.get('gnuhealth.insurance').__table__()

        join1 = Join(hospitalization, patient)
        join1.condition = hospitalization.patient == join1.right.id
        join2 = Join(join1, party)
        join2.condition = join1.right.name == join2.right.id
        join3 = Join(join2, insurance)
        join3.condition = join1.right.current_insurance == join3.right.id
        where = Literal(True)

        if (Transaction().context.get('date_start') and
            Transaction().context.get('date_end')):
            where &= (((hospitalization.state == 'hospitalized') &
                ((hospitalization.hospitalization_date <
                    Transaction().context['date_end'] + timedelta(days=1)) |
                ((hospitalization.hospitalization_date >=
                    Transaction().context['date_start']) &
                (hospitalization.hospitalization_date <
                    Transaction().context['date_end'] + timedelta(days=1)))))
                     |
                ((hospitalization.state == 'done') &
                ((hospitalization.hospitalization_date <
                    Transaction().context['date_end'] + timedelta(days=1)) &
                (hospitalization.effective_discharge_date >=
                    Transaction().context['date_start']))))
        if Transaction().context.get('healthprof'):
            where &= hospitalization.attending_physician == \
                Transaction().context['healthprof']
        if Transaction().context.get('insurance'):
            where &= \
                join3.right.company == Transaction().context['insurance']
        if Transaction().context.get('patient'):
            where &= \
                hospitalization.patient == Transaction().context['patient']

        return join3.select(
            hospitalization.id,
            hospitalization.create_uid,
            hospitalization.create_date,
            hospitalization.write_uid,
            hospitalization.write_date,
            hospitalization.name,
            join2.right.ref,
            join1.right.id.as_('patient'),
            join3.right.number,
            join2.right.gender,
            hospitalization.hospitalization_date,
            hospitalization.effective_discharge_date.as_('discharge_date'),
            hospitalization.attending_physician,
            hospitalization.admission_reason,
            hospitalization.state,
            where=where)

    def get_address(self, name):
        res = ''
        if self.patient.name.addresses:
            res = self.patient.name.addresses[0].full_address
        return res

    def get_insurance(self, name):
        res = ''
        if self.patient.current_insurance:
            res = self.patient.current_insurance.company.name
        return res


class OpenHospitalizationReportStart(ModelView):
    'Open Hospitalization Report'
    __name__ = 'gnuhealth.hospitalization.report.open.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Health Prof')
    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    insurance = fields.Many2One('party.party', 'Insurance',
        domain=([('is_insurance_company', '=', True)]))

    @staticmethod
    def default_date_start():
        return datetime.now()

    @staticmethod
    def default_date_end():
        return datetime.now()


class OpenHospitalizationReport(Wizard):
    'Open Hospitalization Report'
    __name__ = 'gnuhealth.hospitalization.report.open'

    start = StateView(
        'gnuhealth.hospitalization.report.open.start',
        'Lister.hospitalization_report_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open_', 'tryton-ok', default=True),
            ])
    open_ = StateAction('Lister.act_hospitalization_report_view_tree')

    def do_open_(self, action):
        context = {}
        context['date_start'] = self.start.date_start
        context['date_end'] = self.start.date_end
        if self.start.healthprof:
            context['healthprof'] = self.start.healthprof.id
        if self.start.insurance:
            context['insurance'] = self.start.insurance.id
        if self.start.patient:
            context['patient'] = self.start.patient.id
        action['pyson_context'] = PYSONEncoder().encode(context)
        return action, {}

    def transition_open_(self):
        return 'end'


class OpenInpatientEvaluations(Wizard):
    'Open Inpatient Evaluation'
    __name__ = 'wizard.gnuhealth.inpatient.evaluation'
  
    start_state = 'inpatient_evaluation'
    inpatient_evaluation = StateAction('Lister.act_evaluation_form1')

    def do_inpatient_evaluation(self, action):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')

        inpatient = Inpatient(Transaction().context.get('active_id'))
        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', inpatient.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': inpatient.patient.id,
            })
            
        return action, {}
